<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ResortController;
use App\Http\Controllers\UserController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
 */

Route::group(['middleware' => 'api'], function () {
    Route::post('login', [UserController::class, 'login']);
    Route::post('registration', [UserController::class, 'registration']);
    Route::get('me', [UserController::class, 'me']);
    Route::get('logout', [UserController::class, 'logout']);

    Route::group(['middleware' => 'jwt.auth'], function () {
        Route::get('resorts', [ResortController::class, 'getAllResorts']);
        Route::post('favorites', [ResortController::class, 'addToFavorites']);
        Route::get('user-favorites', [ResortController::class, 'getUserFavorites']);
        Route::delete('favorites', [ResortController::class, 'removeToFavorites']);
    });
});
