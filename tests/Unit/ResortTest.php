<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Models\Resort;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ResortTest extends TestCase
{
    use RefreshDatabase;

    public function test_add_to_favorites()
    {
        $user = User::factory()->create();
        $resort = Resort::factory()->create();

        // получение JWT-токена для пользователя (actingAs($user))
        $token = auth()->tokenById($user->id);

        $this->withHeaders(['Authorization' => 'Bearer ' . $token])
            ->json('POST', 'api/favorites', ['resort_ids' => [$resort->id]]);
        $response = $this->withHeaders(['Authorization' => 'Bearer ' . $token])
            ->get('api/user-favorites');

        $response->assertJson([
            'user_favorites' => [$resort->id],
        ]);
    }
}
