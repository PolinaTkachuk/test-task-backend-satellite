<?php

namespace App\Rules;

use Closure;
use Illuminate\Contracts\Validation\ValidationRule;
use App\Models\User;
use Illuminate\Translation\PotentiallyTranslatedString;

class LimitFavoriteResortRule implements ValidationRule
{
    private $userId;

    public function __construct($userId)
    {
        $this->userId = $userId;
    }

    /**
     * Определить, пройдено ли правило валидации:у 1 пользователя не более 3 мест в желаемом
     * текущие желаемые места пользователя и желаемые на добавление места не превышают 3
     * @param  mixed  $value
     * @return bool
     */
    public function passes(mixed $value): bool
    {
        $userResortsCount = User::find($this->userId)->resorts->count();

        return ($userResortsCount + count($value)) <= 3;
    }

    /**
     * Run the validation rule.
     *
     * @param  \Closure(string): PotentiallyTranslatedString  $fail
     * @return void
     */
    public function validate(string $attribute, mixed $value, Closure $fail): void
    {
        if(!$this->passes($value)) {
            $fail('Error:Exceeding the number of favorite resorts for a user');
        }
    }
}
