<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class User
 *
 * @property int $id
 * @property string $title
 * @property float $latitude
 * @property float $longitude
 */

class Resort extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'id',
        'title',
        'latitude',
        'longitude',
    ];

    public function users(): BelongsToMany
    {
        return $this->belongsToMany(User::class, 'favorites')->using(Favorite::class);
    }
}
