<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\Pivot;

/**
 * Class User
 *
 * @property int $id
 * @property string $user_id
 * @property string $resort_id
 */
class Favorite extends Pivot
{
    use HasFactory;
    /**
     * Указывает, что идентификаторы модели являются автоинкрементными.
     *
     * @var bool
     */
    public $incrementing = true;

    protected $table ='favorites';
    protected $fillable = [
        'id',
        'user_id',
        'resort_id',
    ];

}
