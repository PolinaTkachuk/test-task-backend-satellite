<?php

namespace App\Scribe;

use Knuckles\Camel\Extraction\ExtractedEndpointData;
use Knuckles\Scribe\Extracting\Strategies\Strategy;
use App\Models\User;

class AddJwtToken extends Strategy
{
    public function __invoke(ExtractedEndpointData $endpointData, array $routeRules = []): array
    {
        $route = $endpointData->route;
        //добавление токена jwt в заголовок для Route::group(['middleware'=> 'jwt.auth']) и машрутов для которых нелбходим jwt
        if ($this->routeRequiresAuthentication($route)) {
            $user = User::factory()->create();
            $token = auth()->tokenById($user->id);
            $endpointData->headers = ['Authorization' => 'Bearer ' . $token];
        }

        return [];
        // return $endpointData->toArray();
    }

    protected function routeRequiresAuthentication($route): bool
    {
        $middlewares = $route->action['middleware'];

        return in_array('jwt.auth', $middlewares) || in_array($route->getName(), ['api/me', 'api/logout']);
    }
}
