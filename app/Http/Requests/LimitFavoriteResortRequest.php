<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\ValidationRule;
use Illuminate\Foundation\Http\FormRequest;
use App\Rules\LimitFavoriteResortRule;

class LimitFavoriteResortRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return (bool) auth()->user();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'resort_ids' => ['required', 'array', 'max:3', new LimitFavoriteResortRule(auth()->id())]
        ];
    }

    /**
     * Get error messages to test individual rules.
     *
     * @return array
     */
    public function messages(): array
    {
        return [
            'resort_ids.required' => 'Error:A favorites is required',
            'resort_ids.max:3'    => 'Error:More than 3 places to stay',
        ];
    }
}
