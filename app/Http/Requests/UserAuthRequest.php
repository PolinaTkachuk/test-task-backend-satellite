<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\ValidationRule;
use Illuminate\Foundation\Http\FormRequest;

class UserAuthRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'email'    => 'required|email',
            'password' => 'required|string',
        ];
    }

    /**
     * Get error messages to test individual rules.
     *
     * @return array
     */
    public function messages(): array
    {
        return [
            'email.required'    => 'Error:Email is required',
            'email.email'       => 'Error:Incorrect email',
            'password.required' => 'Error:Password is required',
            'password.string'   => 'Error:Incorrect password',
        ];
    }
}
