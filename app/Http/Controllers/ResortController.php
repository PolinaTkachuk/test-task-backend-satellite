<?php

namespace App\Http\Controllers;

use App\Models\Resort;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use App\Http\Requests\LimitFavoriteResortRequest;

class ResortController extends Controller
{
    /**
     * @group Resorts
     * @authenticated
     * @response {"resorts": [{"id":1,"title": "Officia suscipit odio impedit neque.","latitude": "-46.893567","longitude": "152.556851",]}}
     * Метод возвращает в виде json места для отдыха.
     */
    public function getAllResorts(): JsonResponse
    {
        return response()->json([
            'resorts' => Resort::all()
        ]);
    }

    /**
     * @group Resorts
     * @authenticated
     * @bodyParam resort_ids Массив ID курортов, которые необходимо добавить в избранное. Example: [1, 2]
     * @response 200 {void}
     * Метод возвращает пустой ответ при успешном добавлении курортов в избранное.
     */
    public function addToFavorites(LimitFavoriteResortRequest $request): void
    {
        User::find(auth()->id())->resorts()->attach(collect($request->input('resort_ids'))->map(function ($request_id) {
            return $request_id;
        }));
    }

    /**
     * @group Resorts
     * @authenticated
     * @response {"user_favorites": [1, 2]}
     * Метод возвращает в виде json id избранных мест отдыха для пользователя.
     */
    public function getUserFavorites(): JsonResponse
    {
        $resorts = User::find(auth()->id())->resorts()->orderBy('id')->get();

        return response()->json([
            'user_favorites' => $resorts->pluck('pivot.resort_id')
        ]);
    }

    /**
     * @group Resorts
     * @authenticated
     * @bodyParam resort_ids Массив ID курортов, которые необходимо удалить из избранного. Example: [1, 2]
     * @response 200 {void}
     * Метод возвращает пустой ответ при успешном удалении курортов из избранного.
     */
    public function removeToFavorites(LimitFavoriteResortRequest $request): void
    {
        User::find(auth()->id())->resorts()->detach(collect($request)->map(function ($request_id) {
            return $request_id;
        }));
    }
}
