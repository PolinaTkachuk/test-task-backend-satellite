<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserRegisterRequest;
use Illuminate\Http\JsonResponse;
use App\Http\Requests\UserAuthRequest;
use App\Models\User;
use Tymon\JWTAuth\JWTGuard;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['login', 'registration', 'refresh']]);
    }

    /**
     * @group Users
     * @bodyParam form-data  Example: [ "email" => "example@bk.ru", "password" => "12345678"]
     * @response {"access_token": $token ,  "token_type": "bearer", "expires_in": 123600}
     * Метод возвращает в виде json токен, тип токена, время жизни токена
     */
    public function login(UserAuthRequest $request): JsonResponse|array
    {
        $credentials = ['email' => $request->input('email'), 'password' => $request->input('password')];
        /** @var JWTGuard $token **/
        $guard = auth('api');
        $token = $guard->attempt($credentials);

        if (!$token) {
            return $this->respondWithToken(auth()->refresh());
        }

        return $this->respondWithToken($token);
    }

    /**
     * @group Users
     * @bodyParam form-data  Example: [ "name" => "name", "email" => "example@bk.ru", "password" => "12345678"]
     * @response {"access_token": $token ,  "token_type": "bearer", "expires_in": 123600}
     * Метод возвращает в виде json токен, тип токена, время жизни токена
     */
    public function registration(UserRegisterRequest $request): JsonResponse|array
    {
        if (User::where('email', $request['email'])->exists()) {
            return response()->json(['error' => 'user already registered']);
        }

        $user = User::create([
            'name'     => $request->input('name'),
            'email'    => $request->input('email'),
            'password' => Hash::make($request->input('password'))
        ]);

        $token = auth()->tokenById($user->id);

        return $this->respondWithToken($token);
    }

    /**
     * @group Users
     * @response 200 {"me": {"id": 145, "name": "Ksy", "email": "ksyy@bk.ru", "email_verified_at": null }
     * @response 401 {"message": "Unauthenticated."}
     * @return JsonResponse
     */
    public function me(): JsonResponse
    {
        return response()->json(['me' => auth()->user()]);
    }

    /**
     * @group Users
     * @response 200 ['message' => 'Successfully logged out']
     */
    public function logout(): JsonResponse
    {
        auth()->logout();

        return response()->json(['message' => 'Successfully logged out']);
    }

    /**
     * @group Users
     * @return JsonResponse
     */
    public function refresh(): JsonResponse
    {
        return $this->respondWithToken(auth()->refresh());
    }

    /**
     * Get the token array structure.
     * @group Users
     * @bodyParam string $token
     * @response {"access_token": $token ,  "token_type": "bearer", "expires_in": 123600}
     */
    protected function respondWithToken($token): JsonResponse
    {
        return response()->json([
            'access_token' => $token,
            'token_type'   => 'bearer',
            'expires_in'   => auth()->factory()->getTTL() * 60
        ]);
    }
}
