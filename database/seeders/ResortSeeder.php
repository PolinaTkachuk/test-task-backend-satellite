<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Resort;

class ResortSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        //создание 25+75 мест отдыха
        Resort::factory(100)->create();
    }
}
