<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Resort;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        User::factory(25)->create(); //25 пользователей без связей с местами отдыха
        User::factory(25)->create()->each(function ($user) { //с каждым из 25 пользователей связываем от 1 до 3 мест
            $user->resorts()->saveMany(Resort::factory(rand(1, 3))->make());
        });
    }
}
